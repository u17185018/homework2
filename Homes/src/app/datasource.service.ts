import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {People,Role,User,Usertype,Company} from './front/front.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
const httpOptions = {
  headers: new HttpHeaders({
    "Access-Control-Allow-Origin": '*'
  })
}
@Injectable({
  providedIn: 'root'
})
export class DatasourceService {


  formData = new People();
  data:People[];
  paginator: MatPaginator;
  sort: MatSort;
  constructor(private http: HttpClient) { }
  connect(): Observable<People[]> {

    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }
  disconnect() {}
  private getPagedData(data: People[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  private getSortedData(data: People[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }
    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'Name': return compare(a.Name, b.Name, isAsc);
        case 'Surname': return compare(+a.Surname, +b.Surname, isAsc);
        default: return 0;
      }

    });
    function compare(a: string | number, b: string | number, isAsc: boolean) {
      return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }
  }
  
  
  getUsers(): Observable<People[]>{
    return this.http.get<People[]>('http://localhost:50325/api/Security/GetPersonData');
    
  }
  getRole(): Observable<Role[]>{
    return this.http.get<Role[]>('http://localhost:50325/api/Security/GetRoleDatas');
    
  }
  getCompany(): Observable<Company[]>{
    return this.http.get<Company[]>('http://localhost:50325/api/Security/GetCompanyData');
  }
  getUser(): Observable<User[]>{
    return this.http.get<User[]>('http://localhost:50325/api/Security/GetUserData');
  }
  getUsertype(): Observable<Usertype[]>{
    return this.http.get<Usertype[]>('http://localhost:50325/api/Security/GetUsertypeData');
  }

  postPerson(formData: People){
    return this.http.post('http://localhost:50325/api/Security/AddpersonData', formData);
  }


  deleteUsers(id: number) {
    return this.http.delete('http://localhost:50325/api/Security/'+id)
  }
  deleteRole(id: number) {
    return this.http.delete('http://localhost:50325/api/Security/Role/'+id)
  }
  deleteCompany(id: number) {
    return this.http.delete('http://localhost:50325/api/Security/Company/'+id)
  }
  deleteUser(id: number) {
    return this.http.delete('http://localhost:50325/api/Security/Users/'+id)
  }
  deleteUsertype(id: number) {
    return this.http.delete('http://localhost:50325/api/Security/Usertype/'+id)
  }

  updateUsers(formData :People, id:number){ 
  return this.http.post('http://localhost:50325/api/Security/'+id, formData, httpOptions)

  }
  

  doSomething(): Observable<People[]> {
  return this.http.get<People[]>('http://localhost:50325/api/Security/GetPersonData');
  }

}
