import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { FrontComponent } from './front/front.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';
import { UsersComponent } from './users/users.component'
import { RouterModule, Routes } from '@angular/router';
import { CompanyComponent } from './company/company.component';
import { UsertypeComponent } from './usertype/usertype.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgForm} from '@angular/forms';
import { CopyComponent } from './copy/copy.component';

const appRoutes: Routes = [
  {path : '', component: LoginComponent },
  {path : 'front', component: FrontComponent },
  {path : 'user', component: UsersComponent },
  {path : 'usertype', component: UsertypeComponent}
  ];

@NgModule({
  declarations: [
    AppComponent,
    FrontComponent,
    LoginComponent,
    NavComponent,
    UsersComponent,
    CompanyComponent,
    UsertypeComponent,
    CopyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
