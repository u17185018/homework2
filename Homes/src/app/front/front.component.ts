import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DatasourceService } from '../datasource.service';
import { Observable } from 'rxjs';
import {DataSource} from '@angular/cdk/collections';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { NgForm } from '@angular/forms';
import { MatSort } from '@angular/material/sort';



export class People{
  ID : number;
  Name : string;
  Surname : string;
}
export class Role{
  ID : number;
  Name : string;
}
export class User{
  ID : number;
  Username : string;
  Password : string;
}
export class Usertype {
  ID : number;
  Description : string;
}

export class Company{
  ID : number;
  Name : string;
}

@Component({
  selector: 'app-front',
  templateUrl: './front.component.html',
  styleUrls: ['./front.component.scss']
})

export class FrontComponent implements  AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<People>;
  people : People[];
  selectedPerson : any;
  displayedColumns: string[] = ['id','name','surname','Actions'];
  
  dataSource = new MatTableDataSource<People>();
  


  constructor(public data : DatasourceService) { }


  deleteUsers(id:number){
    this.selectedPerson = id;
    console.log(this.selectedPerson);
  }
  removeUser(id:number){
  console.log(id)
    this.data.deleteUsers(id).subscribe(res => {
      this.selectedPerson = null; 
      this.refresh();
      this.data.getUsers();
    })
  }

  updateUser(pe : People ){
    this.selectedPerson = pe;
    console.log(this.selectedPerson);
  }

  ngOnInit(){
    this.data.getUsers().subscribe(res => {
      this.people = res;
    this.dataSource = new MatTableDataSource<People>(this.people);
    this.dataSource.data = res;
    })

  }
  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
  resetForm(form?: NgForm){
    if(form != null)
    form.resetForm();

    this.data.formData = {
      ID : null,
      Name: '',
      Surname: '',
    }
  }
  Update( pe : People){
   this.data.updateUsers(pe,pe.ID) .subscribe(res => {
     this.selectedPerson = null;
     this.data.getUsers();
   })
  }
  onSubmit(form : NgForm){
    this.data.postPerson(this.data.formData).subscribe(res => {
      this.resetForm(form);
      this.refresh();
      this.data.getUsers();
    })
  }
  refresh() {
    this.data.doSomething().subscribe((data: People[]) => {
      this.dataSource.data = data;
    });
  }
}







