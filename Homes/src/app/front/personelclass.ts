import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { IPersonel } from './personel';
import { Observable } from 'rxjs';


@Injectable()
export class PersonelService{
  constructor(private http : HttpClient){ }

  private _url : string = "localhost:50325/api/Security/GetPersonData";

getPersonel() : Observable<IPersonel[]> {
   return this.http.get<IPersonel[]>(this._url);
 }

}