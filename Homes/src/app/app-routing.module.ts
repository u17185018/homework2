import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FrontComponent } from './front/front.component';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';


const Routes: Routes = [
  {path : '', component: LoginComponent },
  {path : 'front', component: FrontComponent },
  {path : 'user', component: UsersComponent }
  ];

@NgModule({
  imports: [RouterModule.forRoot(Routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
