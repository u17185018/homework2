import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { DatasourceService } from '../datasource.service';

export class User{
  ID : number;
  Username : string;
  Password : string;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UsersComponent implements OnInit {
  users : User[];
  displayedColumn: string[] = ['id','username','password','Actions'];
  dataSources = new MatTableDataSource<User>();
  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(public data : DatasourceService) { }
  deleteUser(id:number){
    console.log(id)
    this.data.deleteUser(id).subscribe(res => {
      this.data.getUser();
    })
  }

  ngOnInit(){
    this.data.getUser().subscribe(res => {
      this.users = res;
    this.dataSources = new MatTableDataSource<User>(this.users);
    this.dataSources.data = res;
    })


  }
}

