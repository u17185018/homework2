import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { DatasourceService} from '../datasource.service';
import { NgForm } from '@angular/forms';
export class People{
  ID : number;
  Name : string;
  Surname : string;
}
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  people : People[];
  selectedPerson : any;
  constructor( public data : DatasourceService, private http : HttpClient ) {}
  resetForm(form?: NgForm){
    if(form != null)
    form.resetForm();

    this.data.formData = {
      ID : null,
      Name: '',
      Surname: '',
    }
  }
  onSubmit(form : NgForm){
    this.data.postPerson(this.data.formData).subscribe(res => {
      this.refresh();
      this.resetForm(form);
      
    })
  }
  ngOnInit (){
    this.data.getUsers().subscribe(res => {
      this.people = res;  
    })
  }
  refresh(){this.data.getUsers().subscribe(res => {
    this.people = res;
  })
  }
//How to refresh between pages
}
