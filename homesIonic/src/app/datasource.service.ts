import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
const httpOptions = {
  headers: new HttpHeaders({
    "Access-Control-Allow-Origin": '*'
  })
}
export class People{
  ID : number;
  Name : string;
  Surname : string;
}
@Injectable({
  providedIn: 'root'
})
export class DatasourceService {

  constructor(private http: HttpClient) { }
  formData = new People();
  data:People[];

  getUsers(): Observable<People[]>{
    return this.http.get<People[]>('http://localhost:50325/api/Security/GetPersonData');
    
  }

  postPerson(formData: People){
    return this.http.post('http://localhost:50325/api/Security/AddpersonData', formData);
  }
  updateUsers(formData :People, id:number){ 
    return this.http.post('http://localhost:50325/api/Security/'+id, formData, httpOptions)
  
    }
    deleteUsers(id: number) {
      return this.http.delete('http://localhost:50325/api/Security/'+id)
    }
}




  