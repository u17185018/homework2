import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { DatasourceService} from '../datasource.service';
export class People{
  ID : number;
  Name : string;
  Surname : string;
}
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  people : People[];
  selectedPerson : any;
  constructor( public data : DatasourceService, private http : HttpClient ) {}
  ngOnInit (){
    this.data.getUsers().subscribe(res => {
      this.people = res;
      
    })
  }
  refresh(){this.data.getUsers().subscribe(res => {
    this.people = res;
  })
  }
  doSomething(ref:any){
    this.refresh();
  }
}
