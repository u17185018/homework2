import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DatasourceService} from '../datasource.service';
export class People{
  ID : number;
  Name : string;
  Surname : string;
}
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  people : People[];
  selectedPerson : any;
  constructor( public data : DatasourceService, private http : HttpClient ) {}
  deleteUsers(id:number){
    this.selectedPerson = id;
    console.log(this.selectedPerson);
  }
  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
    this.refresh();
  }
  updateUser(pe : People ){
    this.selectedPerson = pe;
    console.log(this.selectedPerson);
  }
  Update( pe : People){
    this.data.updateUsers(pe,pe.ID) .subscribe(res => {
      this.selectedPerson = null;
      this.refresh();
      this.data.getUsers();
    })
   } 
  removeUser(id:number){
    console.log(id)
      this.data.deleteUsers(id).subscribe(res => {
        this.selectedPerson = null; 
        this.refresh();
        this.data.getUsers();
      })
    }
  
ngOnInit (){
  this.data.getUsers().subscribe(res => {
    this.people = res;
    
  })
  this.refreshData();
}
ngOnDestroy(){
  clearInterval(this.intervalUpdate)
}
ionViewDidLeave() {
  this.people = null;
  
}
ionViewDidEnter(){
  this.refresh();
}
refresh(){this.data.getUsers().subscribe(res => {
  this.people = res;
})
}
intervalUpdate;
refreshData() {
  this.intervalUpdate = setInterval(() => {
    this.data.getUsers().toPromise().then(data => {
      this.people = data
    })
  },5000)}
}