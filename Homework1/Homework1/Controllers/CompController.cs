﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Homework1.Models;
using System.Data.Entity;
using System.Dynamic;
using System.Web.Http;
using System.Web.Http.Cors;


namespace Homework1.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods:"*")]
    public class CompController : System.Web.Http.ApiController
    {
        //getRole

        [System.Web.Http.Route("api/Security/GetRoleData")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetRoleData()
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;


            return GetRoleList(db.Roles.ToList());
        }
        private List<dynamic> GetRoleList(List<Role> forclient)
        {
            List<dynamic> dynamicRoles = new List<dynamic>();
            foreach (Role t in forclient)
            {
                dynamic dynamicRole = new ExpandoObject();
                dynamicRole.ID = t.id;
                dynamicRole.Description = t.Descriptions;
                dynamicRoles.Add(dynamicRole);

            }
            return dynamicRoles;
        }
        //get Usertype
        [System.Web.Http.Route("api/Security/GetUsertypeData")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetUsertypeData()
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;


            return GetUsertypeList(db.Usertypes.ToList());
        }
        private List<dynamic> GetUsertypeList(List<Usertype> forclient)
        {
            List<dynamic> dynamicRoles = new List<dynamic>();
            foreach (Usertype t in forclient)
            {
                dynamic dynamicRole = new ExpandoObject();
                dynamicRole.ID = t.ID;
                dynamicRole.Description = t.Descript;
                dynamicRoles.Add(dynamicRole);

            }
            return dynamicRoles;
        }
        //GetUser
        [System.Web.Http.Route("api/Security/GetUserData")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetUserData()
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;


            return GetUserList(db.Users.ToList());
        }
        private List<dynamic> GetUserList(List<User> forclient)
        {
            List<dynamic> dynamicRoles = new List<dynamic>();
            foreach (User t in forclient)
            {
                dynamic dynamicRole = new ExpandoObject();
                dynamicRole.ID = t.ID;
                dynamicRole.Username = t.Username;
                dynamicRole.Password = t.Passwords;
                dynamicRoles.Add(dynamicRole);

            }
            return dynamicRoles;
        }
        //GetCompany
        [System.Web.Http.Route("api/Security/GetCompanyData")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetCompanyData()
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;


            return GetCompanyList(db.Companies.ToList());
        }
        private List<dynamic> GetCompanyList(List<Company> forclient)
        {
            List<dynamic> dynamicRoles = new List<dynamic>();
            foreach (Company t in forclient)
            {
                dynamic dynamicRole = new ExpandoObject();
                dynamicRole.ID = t.id;
                dynamicRole.Name = t.Name;
                dynamicRoles.Add(dynamicRole);

            }
            return dynamicRoles;
        }
        //GetPerson
        [System.Web.Http.Route("api/Security/GetPersonData")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetPersonData()
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;


            return GetPeopleList(db.People.ToList());
        }
        private List<dynamic> GetPeopleList(List<Person> forclient)
        {
            List<dynamic> dynamicRoles = new List<dynamic>();
            foreach (Person t in forclient)
            {
                dynamic dynamicRole = new ExpandoObject();
                dynamicRole.ID = t.id;
                dynamicRole.Name = t.Name;
                dynamicRole.Surname = t.surname;
                dynamicRoles.Add(dynamicRole);

            }
            return dynamicRoles;
        }
        //get Role
        [System.Web.Http.Route("api/Security/GetRoleDatas")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetRoleDatas()
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;


            return GetRoleLists(db.Roles.ToList());
        }
        private List<dynamic> GetRoleLists(List<Role> forclient)
        {
            List<dynamic> dynamicRoles = new List<dynamic>();
            foreach (Role t in forclient)
            {
                dynamic dynamicRole = new ExpandoObject();
                dynamicRole.ID = t.id;
                dynamicRole.Name = t.Descriptions;
                dynamicRoles.Add(dynamicRole);

            }
            return dynamicRoles;
        }

        //Get Role-Person
        [System.Web.Http.Route("api/Security/GetRolePersonData")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> GetRolePersonData()
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            List<Role> role = db.Roles.Include(zz => zz.People).ToList();


            return GetRolePeopleList(role);
        }
        private List<dynamic> GetRolePeopleList(List<Role> forclient)
        {
            List<dynamic> dynamicRoles = new List<dynamic>();
            foreach (Role t in forclient)
            {
                dynamic ObForClient = new ExpandoObject();
                ObForClient.ID = t.id;
                ObForClient.Description = t.Descriptions;
               ObForClient.People = GetPeople(t);
                dynamicRoles.Add(ObForClient);

            }
            return dynamicRoles;

        }
        private List<dynamic> GetPeople( Role roles)
        {
            List<dynamic> Getpeeps = new List<dynamic>();
            foreach(Person Person in roles.People)
            {
                dynamic dynamicpeep = new ExpandoObject();
                dynamicpeep.id = Person.id;
                dynamicpeep.Name = Person.Name;
                dynamicpeep.surname = Person.surname;
                Getpeeps.Add(dynamicpeep);
            }
            return Getpeeps;
        }


        //Add Person
        [System.Web.Http.Route("api/Security/AddPersonData")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddPerson([FromBody] List<Person> Person)
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            db.People.AddRange(Person);
            db.SaveChanges();
            return GetPersonData();
        }

        //Add User
        [System.Web.Http.Route("api/Security/AddUsersData")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddUsers([FromBody] List<User> Person)
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            db.Users.AddRange(Person);
            db.SaveChanges();
            return GetUserData();
        }
        //Add Usertype
        [System.Web.Http.Route("api/Security/AddUsertypeData")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddUsertype([FromBody] List<Usertype> Person)
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            db.Usertypes.AddRange(Person);
            db.SaveChanges();
            return GetUserData();
        }
        //Add Roles
        [System.Web.Http.Route("api/Security/AddRoleData")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddRole([FromBody] List<Role> Person)
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            db.Roles.AddRange(Person);
            db.SaveChanges();
            return GetRoleData();
        }
        //Add Company
        [System.Web.Http.Route("api/Security/AddCompanyData")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> AddCompany([FromBody] List<Company> Person)
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            db.Companies.AddRange(Person);
            db.SaveChanges();
            return GetCompanyData();
        }



        //DeletePerson
        [System.Web.Http.Route("api/Security/{Id}")]
        [System.Web.Mvc.HttpPost]
        public void Delete( int Id)
        {
            using (CompaniesEntities1 db = new CompaniesEntities1())
            {
                db.People.Remove(db.People.FirstOrDefault(pp => pp.id == Id));
                db.SaveChanges();
            }
        }
        //DeleteRoles
        [System.Web.Http.Route("api/Security/Role/{Id}")]
        [System.Web.Mvc.HttpPost]
        public void DeleteRole(int Id)
        {
            using (CompaniesEntities1 db = new CompaniesEntities1())
            {
                db.Roles.Remove(db.Roles.FirstOrDefault(pp => pp.id == Id));
                db.SaveChanges();
            }
        }
        //DeleteCompany
        [System.Web.Http.Route("api/Security/Company/{Id}")]
        [System.Web.Mvc.HttpPost]
        public void DeleteCompany(int Id)
        {
            using (CompaniesEntities1 db = new CompaniesEntities1())
            {
                db.Companies.Remove(db.Companies.FirstOrDefault(pp => pp.id == Id));
                db.SaveChanges();
            }
        }
        //DeleteUser
        [System.Web.Http.Route("api/Security/Users/{Id}")]
        [System.Web.Mvc.HttpPost]
        public void DeleteUsers(int Id)
        {
            using (CompaniesEntities1 db = new CompaniesEntities1())
            {
                db.Users.Remove(db.Users.FirstOrDefault(pp => pp.ID == Id));
                db.SaveChanges();
            }
        }
        //DeleteUsertype
        [System.Web.Http.Route("api/Security/Usertype/{Id}")]
        [System.Web.Mvc.HttpPost]
        public void DeleteUsertype(int Id)
        {
            using (CompaniesEntities1 db = new CompaniesEntities1())
            {
                db.Usertypes.Remove(db.Usertypes.FirstOrDefault(pp => pp.ID == Id));
                db.SaveChanges();
            }
        }



        //update Person
        [System.Web.Http.Route("api/Security/{Id}")]
        [System.Web.Mvc.HttpPost]

    public List<dynamic> Put(int Id,[FromBody] Person Person)
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            var Persons = db.People.FirstOrDefault(pp => pp.id == Id);
            Persons.Name = Person.Name;
            Persons.surname = Person.surname;
            Persons.RoleID = Person.RoleID;
            db.SaveChanges();

            return GetRolePersonData();
        }

        [System.Web.Http.Route("api/Security/UpdateRoles/{Id}")]
        [System.Web.Mvc.HttpPost]

        //Update Roles
        public List<dynamic> PutRoles(int Id, [FromBody] Role Person)
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            var Persons = db.Roles.FirstOrDefault(pp => pp.id == Id);
            Persons.Descriptions = Person.Descriptions;
            db.SaveChanges();

            return GetRoleData();
        }
        //Update Company
        [System.Web.Http.Route("api/Security/UpdateCompany/{Id}")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> PutCompany(int Id, [FromBody] Company Person)
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            var Persons = db.Companies.FirstOrDefault(pp => pp.id == Id);
            Persons.Name = Person.Name;
            Persons.PersonID = Person.id;
            db.SaveChanges();

            return GetCompanyData();
        }
        //Update Usertype
        [System.Web.Http.Route("api/Security/UpdateUsertype/{Id}")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> PutUserTypes(int Id, [FromBody] Usertype Person)
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            var Persons = db.Usertypes.FirstOrDefault(pp => pp.ID == Id);
            Persons.Descript = Person.Descript;
            db.SaveChanges();

            return GetUsertypeData();
        }
        //update Users
        [System.Web.Http.Route("api/Security/UpdateUsertypes/{Id}")]
        [System.Web.Mvc.HttpPost]
        public List<dynamic> PutCompany(int Id, [FromBody] User Person)
        {
            CompaniesEntities1 db = new CompaniesEntities1();
            db.Configuration.ProxyCreationEnabled = false;
            var Persons = db.Users.FirstOrDefault(pp => pp.ID == Id);
            Persons.Username = Person.Username;
            Persons.Passwords = Person.Passwords;
            Persons.UsertypeID = Person.UsertypeID;
            db.SaveChanges();

            return GetCompanyData();
        }

    }
}